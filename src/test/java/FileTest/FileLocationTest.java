package FileTest;

import com.app.data.FileLocation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class FileLocationTest {

    FileLocation file = new FileLocation();

    // positive input
    int inputTrue = 1;
    String expectedTrue = "src/main/resources/data_Sekolah.csv";


    // negative input
    int inputFalse = 10;
    int inputMin = -1;
    String inputName = "kelas 1";
    String inputNull = null;

    @Test
    @DisplayName("Positive Test - Input Range Index")
    void testLocation() {
        inputOne();
    }

    void inputOne(){
        String result = file.location(inputTrue);
        Assertions.assertEquals(expectedTrue, result);
    }

    @Test
    @DisplayName("Negative Test - Input Greater than Index")
    void testLocationError() {
        RuntimeException r = Assertions.assertThrows(IndexOutOfBoundsException.class, () -> file.location(inputFalse));
        Assertions.assertEquals("Index Not Found!", r.getMessage());
    }

    @Test
    @DisplayName("Negative Test - Input Smaller than index")
    void testLocationErrorMin() {
        Exception e = Assertions.assertThrows(IndexOutOfBoundsException.class, () -> file.location(inputMin));
        Assertions.assertEquals("Index Can't Minus", e.getMessage());
    }

    @Test
    @DisplayName("Positive Test - Input Name")
    void testNameFile() {
        String result = file.nameFile(inputName);
        Assertions.assertEquals("src/main/savefile/" + inputName + ".txt", result);
    }

    @Test
    @DisplayName("Negative Test - Input Name File Null")
    void testNameFileNull(){
        Exception e = Assertions.assertThrows(NullPointerException.class, () -> file.nameFile(inputNull));
        Assertions.assertEquals("Name File Can't Null", e.getMessage());
    }
}
