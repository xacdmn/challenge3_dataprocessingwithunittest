package com.app.menu;

import java.io.IOException;
import java.util.Scanner;

public abstract class Menu {

    Scanner sc = new Scanner(System.in);
    private final String TITTLE = "Aplikasi Pengolahan Data Nilai Siswa";

    public abstract void page() throws IOException;

    public void header() {
        String space = "   ";
        separatorTitle(TITTLE.length());
        System.out.println(space+TITTLE);
        separatorTitle(TITTLE.length());
    }

    public void footer() {
        int titleLength = TITTLE.length();
        separatorTitle(titleLength);
        String FOOTER = "Masukkan Pilihan -> ";
        System.out.print(FOOTER);
    }

    public String mainPage() {
        header();
        System.out.println("1. Data Frequency");
        System.out.println("2. Data Mean Median Mode");
        System.out.println("0. Exit");
        footer();
        return sc.nextLine();
    }

    public int secondPage() {
        header();
        System.out.println("1. Data Sekolah");
        System.out.println("2. Kelas A");
        System.out.println("3. Kelas B");
        System.out.println("4. Kelas C");
        System.out.println("5. Kelas D");
        System.out.println("6. Kelas E");
        System.out.println("7. Kelas F");
        System.out.println("8. Kelas G");
        System.out.println("9. Kelas H");
        System.out.println("0. Kembali ke menu utama");
        footer();
        return sc.nextInt();
    }

    private void separatorTitle(int size) {
        StringBuilder separator = new StringBuilder();
        for (int i = 0; i < size + 6; i++){
            separator.append("-");
        }
        System.out.println(separator);
    }
}
