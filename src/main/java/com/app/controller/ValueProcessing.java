package com.app.controller;

import com.app.math.MathImp;

import java.io.*;
import java.text.DecimalFormat;

public class ValueProcessing extends ReadAction {

    MathImp search = new MathImp();
    DecimalFormat df = new DecimalFormat("0.00");

    @Override
    public void read() {
    }

    /*---------------------WRITE MEAN MEDIAN MOD----------------------*/
    public void valueProcessing(String path, String nameFile) throws IOException, RuntimeException {
        try {
            File file = new File(nameFile);
            if(path != null && file.createNewFile()) {
                System.out.println("File tersimpan di: " + nameFile);
            } else if (file.exists()){
                System.out.println("File Telah di Update");
                System.out.println("File tersimpan di: " + nameFile);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            bwr.write("Berikut Hasil Pengolahan Nilai:\n");
            bwr.write(" \n");
            bwr.write("Berikut hasil sebaran data nilai\n");


            bwr.write("Mean   : " + df.format(search.mean(readFile(path))) + "\n");
            bwr.write("Median : " + df.format(search.median(readFile(path))) + "\n");
            bwr.write("Modus  : " + df.format(search.mode(readFile(path))) + "\n");

            bwr.flush();
            bwr.close();
        } catch(IOException i) {
            throw new FileNotFoundException("File Not Found!");
        } catch (RuntimeException r) {
            throw new NullPointerException("File is Null");
        }
    }
}
