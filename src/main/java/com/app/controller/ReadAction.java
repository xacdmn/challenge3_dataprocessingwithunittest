package com.app.controller;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ReadAction {

    public abstract void read();

    //-----------------------------READ----------------------------//
    public List<Integer> readFile(String path) throws IOException, RuntimeException {
        try {
            File file = new File(path);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            String line = " ";
            String[] tempArr;

            List<Integer> listValues = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                tempArr = line.split(";");

                for (int i = 0; i < tempArr.length; i++) {
                    if (i > 0) {
                        String temp = tempArr[i];
                        Integer intTemp = Integer.parseInt(temp);
                        listValues.add(intTemp);
                    }
                }
            }
            Collections.sort(listValues);
            br.close();
            return listValues;
        } catch(IOException i) {
            throw new FileNotFoundException("File Not Found!");
        } catch (RuntimeException r) {
            throw new NullPointerException("File is Null");
        }
    }
}
